@extends('layout.default')

@section('navigation')
    @include('components._foreign_navigation')
@endsection

@section('content')
<div class="container-fluid p-0">
    @include('components.terms')
    @include('components.contact')
</div>
@endsection
