
<div  class=" position-relative overflow-hidden" >
    <div id="lib-{{$type}}-holder" class="img-lib d-flex hide-scrollbar transition snap-scroll">
        @foreach($photos as $key => $photo)
        <a data-toggle="modal" class="pointer img-lib-hover transition snap" onclick="setCarouselModal('carousel_img_{{$type}}', {{$key}})" data-target="#static_img_{{$type}}">
            <img  src="{{url('storage/home/'.$photo['img'])}}" class="lib-img-icon transition" alt="">
            <i class="icon icon-zoom center transition text-light"></i>
        </a>
        @endforeach
    </div>

    <a onclick="scrollLiv('lib-{{$type}}-holder', 'left')" class="btn btn-dark btn-round btn-slide vertical-center btn-slider-left "  role="button" >
        <ion-icon class="slider-icon text-light" name="chevron-back-outline"></ion-icon>
    </a>
    <a onclick="scrollLiv('lib-{{$type}}-holder', 'right')" class="btn btn-dark btn-round btn-slide vertical-center btn-slider-right "  role="button" >
        <ion-icon class="slider-icon text-light" name="chevron-forward-outline"></ion-icon>
    </a>

</div>


@push('modais')
<div class="modal fade pr-0" id="static_img_{{$type}}"  role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-full">
        <div class="modal-content bg-transparent">
            <div class="modal-body px-0">
                <div class="d-flex justify-content-end">
                    <button type="button" class="btn btn-light btn-round p-3 mr-4" data-dismiss="modal" aria-label="Close">
                        <i class="icon icon-cross"></i>
                    </button>
    
                </div>
                <div style="width: 100vw" id="carousel_img_{{$type}}" class="carousel position-absolute slide vertical-center" data-ride="carousel">
                    <div class="carousel-inner  overflow-visible">
                       @foreach($photos as $key => $photo)
                        <div class="carousel-item px-200 img-carousel-lib @if($key == 0) active @endif">
                            <img src="{{url('storage/home/'.$photo['img'])}}" class="d-block w-100 img-lib-modal" alt="">
                            <div class="carousel-caption d-none d-md-block img-desc">
                                <h4 class="text-light lib-desc-offset text-uppercase">{{$photo['title']}}</h4>
                            </div>
                        </div>
                        @endforeach 
                    </div>
                    <a class="btn btn-light btn-round btn-slide vertical-center btn-slider-left" href="#carousel_img_{{$type}}" role="button" data-slide="prev">
                        <ion-icon class="slider-icon" name="chevron-back-outline"></ion-icon>
                    </a>
                    <a class="btn btn-light btn-round btn-slide vertical-center btn-slider-right" href="#carousel_img_{{$type}}" role="button" data-slide="next">
                        <ion-icon class="slider-icon" name="chevron-forward-outline"></ion-icon>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

@endpush

