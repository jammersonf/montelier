var APP_DATA = {
  "scenes": [
    {
      "id": "0-fachada",
      "name": "Fachada",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1000,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.3134824291526659,
          "pitch": 0.00034418009178338593,
          "rotation": 5.497787143782138,
          "target": "1-sala"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "1-sala",
      "name": "Sala",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1000,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -3.0478782129100104,
          "pitch": 0.14767242823961624,
          "rotation": 0,
          "target": "2-cozinha"
        },
        {
          "yaw": 2.657243253760001,
          "pitch": 0.01598638358603033,
          "rotation": 5.497787143782138,
          "target": "6-suite"
        },
        {
          "yaw": 2.6028902488491124,
          "pitch": 0.2769248553924939,
          "rotation": 3.9269908169872414,
          "target": "7-adega"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "2-cozinha",
      "name": "Cozinha",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1000,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 0.14428885360334398,
          "pitch": 0.21430778131189676,
          "rotation": 0,
          "target": "1-sala"
        },
        {
          "yaw": -0.5305100423818949,
          "pitch": 0.18005951604336445,
          "rotation": 0,
          "target": "4-quarto-trreo"
        },
        {
          "yaw": 1.526567654079379,
          "pitch": 0.38497387848599374,
          "rotation": 10.210176124166829,
          "target": "7-adega"
        },
        {
          "yaw": 1.6933086112974198,
          "pitch": 0.06884056423728957,
          "rotation": 0,
          "target": "6-suite"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "3-garagem",
      "name": "Garagem",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1000,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.8664119682685687,
          "pitch": -0.09897064943753975,
          "rotation": 0,
          "target": "1-sala"
        },
        {
          "yaw": -1.1441676667282685,
          "pitch": 0.1324310488375815,
          "rotation": 4.71238898038469,
          "target": "7-adega"
        },
        {
          "yaw": -2.8402048159048867,
          "pitch": 0.21174223782564283,
          "rotation": 0,
          "target": "0-fachada"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "4-quarto-trreo",
      "name": "Quarto Térreo",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1000,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 2.8053420972433685,
          "pitch": 0.3655379427566867,
          "rotation": 0,
          "target": "2-cozinha"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "5-quarto-solteiro",
      "name": "Quarto Solteiro",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1000,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 2.9297206278239977,
          "pitch": 0.11257795697935585,
          "rotation": 10.995574287564278,
          "target": "1-sala"
        },
        {
          "yaw": 2.9295200617607607,
          "pitch": 0.00698318813733948,
          "rotation": 1.5707963267948966,
          "target": "6-suite"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "6-suite",
      "name": "Suíte",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1000,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 2.7064903689753077,
          "pitch": 0.06430749557285864,
          "rotation": 4.71238898038469,
          "target": "5-quarto-solteiro"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "7-adega",
      "name": "Adega",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 1000,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -1.797943853381744,
          "pitch": 0.3325549735509057,
          "rotation": 0,
          "target": "3-garagem"
        }
      ],
      "infoHotspots": []
    }
  ],
  "name": "Project Title",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": true,
    "fullscreenButton": false,
    "viewControlButtons": false
  }
};
