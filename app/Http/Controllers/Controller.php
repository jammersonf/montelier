<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, SEOToolsTrait;

    public function img($input, $form = 'img', $folder, $key = '', $originalName = false)
    {
        $imageName = time().$key.'.'.$input[$form]->extension();
        if ($originalName) {
            $imageName = $input[$form]->getClientOriginalname();
        }
        $input[$form]->move(public_path('storage/'.$folder), $imageName);

        $input[$form] = $imageName;

        return $imageName;
    }

    public function seo($title = '', $subtitle = '', $url = '', $img = '')
    {
        $this->seo()->setTitle($title);
        $this->seo()->setDescription($subtitle);

        $this->seo()->opengraph()->setDescription($subtitle);
        $this->seo()->opengraph()->setTitle($title);
        if ($slug) {
        $this->seo()->opengraph()->setUrl($url);
        $this->seo()->opengraph()->addProperty('type', 'article');
        $this->seo()->opengraph()->addProperty('locale', 'pt-br');
        }
        $this->seo()->opengraph()->addProperty('locale:alternate', ['pt-pt', 'en-us']);
        if ($img) {
        $this->seo()->opengraph()->addImage($img, ['height' => 451, 'width' => 800]);
        }
    }

    public function toUsd($input)
    {
        $usd = str_replace('.', '', $input);

        return str_replace(',', '.', $usd);
    }
}
