<?php

namespace App\Http\Controllers;

use App\Models\Construction;
use App\Models\Home1;
use App\Models\Home2;
use App\Models\Support;
use Illuminate\Http\Request;

class SupportController extends Controller
{
    protected $support;

    public function __construct(Support $sp) {
        $this->middleware('auth');
        $this->support = $sp;
    }

    public function show($pp = '')
    {
        $data = $this->support->find(1);
        $title = $pp ? 'Política de privacidade' : 'Fale conosco';

        return view('admin.support.edit', compact('data', 'pp', 'title'));
    }

    public function update(Request $request)
    {
        $input = $request->all();
        if (isset($input['lc_pc'])) {
            $input['lc_pc'] = $this->img($input, 'lc_pc', 'support', 1);
        } else {
            unset($input['lc_pc']);
        }
        if (isset($input['lc_mobile'])) {
            $input['lc_mobile'] = $this->img($input, 'lc_mobile', 'support', 2);
        } else {
            unset($input['lc_mobile']);
        }
        if (isset($input['file1'])) {
            $input['file1'] = $this->img($input, 'file1', 'support', 3, true);
        } else {
            unset($input['file1']);
        }
        if (isset($input['file2'])) {
            $input['file2'] = $this->img($input, 'file2', 'support', 4, true);
        } else {
            unset($input['file2']);
        }
        
        if (isset($input['co_1_ico'])) {
            $input['co_1_ico'] = $this->img($input, 'co_1_ico', 'support', 6);
        } else {
            unset($input['co_1_ico']);
        }
        if (isset($input['co_2_ico'])) {
            $input['co_2_ico'] = $this->img($input, 'co_2_ico', 'support', 7);
        } else {
            unset($input['co_2_ico']);
        }
        if (isset($input['co_3_ico'])) {
            $input['co_3_ico'] = $this->img($input, 'co_3_ico', 'support', 8);
        } else {
            unset($input['co_3_ico']);
        }
        $this->support->find(1)->update($input);
        $pp = isset($input['pc_txt']) ? 'pp' : '';

        toastr()->success('Atualizado com sucesso!');

        return redirect()->route('adm.support.show', $pp);
    }

    public function destroy($id)
    {
        $this->home->find($id)->delete();

        toastr()->success('Apagado com sucesso!');

        return redirect()->route('adm.home2.index');
    }

    public function index(Request $request)
    {
        $home1 = Home1::all()->groupBy('type');
        $home2 = $this->home->with('photos')->get()->toArray();
        $cons['years'] = Construction::all()->pluck('year', 'year')->toArray();
        $cons['months'] = Construction::all()->pluck('month_txt', 'month')->toArray();
        $cons['default'] = Construction::latest()->first()->toArray();
        
        return view('index', compact('home1', 'home2', 'cons'));
    }
    public function terms(Request $request)
    {
        return view('terms');
    }
}
